<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form</title>

    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="vendor/nouislider/nouislider.min.css">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="vendor/swal2/sweetalert2.min.css">
    <meta name="robots" content="noindex, follow">
</head>

<body>
<div class="main">
    <div class="container">
        <div class="signup-content">
            <div class="signup-img">
                <img src="img/bg.jpeg" alt="">
            </div>
            <div class="signup-form">
                <form method="POST" action="{{route('donate')}}" class="register-form" id="register-form">
                    @csrf
                    <div class="form-row">
                        <div class="form-group">
                            <div class="form-input">
                                <label for="first_name" class="required">First name</label>
                                <input type="text" name="first_name" id="first_name" value="{{old('first_name')}}"/>
                            </div>
                            <div class="form-input">
                                <label for="last_name" class="required">Last name</label>
                                <input type="text" name="last_name" id="last_name" value="{{old('last_name')}}"/>
                            </div>
                            <div class="form-input">
                                <label for="company" class="required">Company</label>
                                <input type="text" name="company_name" id="company" value="{{old('company_name')}}"/>
                            </div>
                            <div class="form-input">
                                <label for="email" class="required">Email</label>
                                <input type="text" name="email" id="email" value="{{old('email')}}"/>
                            </div>
                            <div class="form-input">
                                <label for="phone_number" class="required">Phone number</label>
                                <input type="text" name="phone" id="phone_number" value="{{old('phone')}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-select">
                                <div class="label-flex">
                                    <label for="gender" class="required">Gender</label>
                                </div>
                                <div class="select-list">
                                    <select name="gender" id="gender">
                                        <option>&nbsp;</option>
                                        <option value="1">Male</option>
                                        <option value="0">Female</option>
                                    </select>
                                    <input type="hidden" name="gender"/>
                                </div>
                            </div>
                            <div class="form-radio">
                                <div class="label-flex">
                                    <label for="payment" class="required">Payment Mode</label>
                                </div>
                                <div class="form-radio-group">
                                    <div class="form-radio-item">
                                        <input type="radio" name="payment" id="cash" checked value="visa">
                                        <label for="cash">Visa</label>
                                        <span class="check"></span>
                                    </div>
                                    <div class="form-radio-item">
                                        <input type="radio" name="payment" id="cheque" value="master_card">
                                        <label for="cheque">Mastercard</label>
                                        <span class="check"></span>
                                    </div>
                                    <div class="form-radio-item">
                                        <input type="radio" name="payment" id="demand" value="amex">
                                        <label for="demand">Amex</label>
                                        <span class="check"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-input">
                                <label for="card_number" class="required">Card Number</label>
                                <input type="text" name="card_number" id="card_number" value="{{old('card_number')}}"/>
                            </div>
                            <div class="form-input">
                                <label for="expiration" class="required">Expiration</label>
                                <input type="text" name="expiration" id="expiration" value="{{old('expiration')}}"/>
                            </div>
                            <div class="form-input">
                                <label for="payable" class="required">CVN</label>
                                <input type="text" name="cvn" id="payable" value="{{old('cvn')}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="donate-us">
                        <label class="required">Donate us</label>
                        <div class="price_slider ui-slider ui-slider-horizontal">
                            <div id="slider-margin"></div>
                            <span class="donate-value" id="value-lower"></span>
                            <input type="hidden" name="donation" id="donation">
                        </div>
                    </div>
                    <div class="form-submit">
                        <input type="submit" value="Submit" class="submit" id="submit" name="submit" />
                        <input type="reset" value="Reset" class="submit" id="reset" name="reset" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/nouislider/nouislider.min.js"></script>
<script src="/vendor/wnumb/wNumb.js"></script>
<script src="/vendor/swal2/sweetalert2.min.js"></script>
<script src="/vendor/inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="/js/main.js"></script>
<script>
    @if($errors->any())
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    Toast.fire({
        icon: 'error',
        title: '{{$errors->first()}}'
    });
    @endif

    @if(session('success'))
    Swal.fire({
        title: 'Success',
        text: '{{session('success')}}',
        icon: 'success'
    });
    @endif
</script>
</body>

</html>
