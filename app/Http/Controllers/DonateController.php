<?php

namespace App\Http\Controllers;

use App\Models\Donation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class DonateController extends Controller
{
    public function donationView()
    {
        return view('index');
    }

    public function donate(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'company_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|regex:/^0[0-9]{9}$/',
            'gender' => 'required|in:0,1',
            'payment' => 'required|in:visa,master_card,amex',
            'card_number' => 'required|max:20',
            'expiration' => 'required|date_format:m/Y',
            'cvn' => 'required',
            'donation' => 'required'
        ]);
        if(!$validator->fails()){
            try{
                $donation = new Donation();
                $donation->store($data);
                return back()->with('success', 'Successful Donation');
            }catch (\Exception $e){
                Log::error('DonationController Error: ' . $e->getMessage());
                return back()->withErrors([
                    'msg' => 'An error has occurred, please contact the administrator for more information'
                ])->withInput();
            }
        }
        return back()->withErrors(['msg' => $validator->errors()->first()])->withInput();
    }
}
