<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    use HasFactory;

    protected $fillable = ['first_name', 'last_name', 'company_name', 'email', 'phone', 'gender', 'payment',
        'card_number', 'expiration', 'cvn', 'donation'];

    public function store($data){
        //convert expire_time
        $data['expiration'] = Carbon::createFromFormat('m/Y', $data['expiration']);

        return $this->create($data);
    }
}
