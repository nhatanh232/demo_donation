(function($) {

  $('#gender').parent().append('<ul class="list-item" id="newgender" name="gender"></ul>');
  $('#gender option').each(function(){
      $('#newgender').append('<li value="' + $(this).val() + '">'+$(this).text()+'</li>');
  });
  $('#gender').remove();
  $('#newgender').attr('id', 'gender');
  $('#gender li').first().addClass('init');
  $("#gender").on("click", ".init", function() {
      $(this).closest("#gender").children('li:not(.init)').toggle();
  });

  var allOptions = $("#gender").children('li:not(.init)');
  $("#gender").on("click", "li:not(.init)", function() {
      allOptions.removeClass('selected');
      $(this).addClass('selected');
      $("#gender").children('.init').html($(this).html());
      allOptions.toggle();
      $('input[type="hidden"][name="gender"]').val($(this).val());
  });

  var marginSlider = document.getElementById('slider-margin');
  if (marginSlider != undefined) {
      noUiSlider.create(marginSlider, {
            start: [500],
            step: 10,
            connect: [true, false],
            tooltips: [true],
            range: {
                'min': 0,
                'max': 1000
            },
            format: wNumb({
                decimals: 0,
                thousand: ',',
                prefix: '$ ',
            })
    });
  }

  $('#reset').on('click', function(){
      marginSlider.noUiSlider.reset();
      allOptions.removeClass('selected');
      $(this).addClass('selected');
      $("#gender").children('.init').html($(this).html());
  });

  marginSlider.noUiSlider.on('update', function(v,h,un){
      $('#donation').val(un[0]);
  });

  $('#card_number').inputmask("9999 9999 9999 9999");
  $('#expiration').inputmask("99/9999");
})(jQuery);
