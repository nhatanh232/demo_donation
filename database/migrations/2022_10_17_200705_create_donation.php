<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->id();
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('company_name');
            $table->string('email');
            $table->string('phone', 20);
            $table->tinyInteger('gender')->default(1);
            $table->enum('payment', ['visa', 'master_card', 'amex'])->default('visa');
            $table->string('card_number', 20);
            $table->timestamp('expiration');
            $table->integer('cvn');
            $table->integer('donation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('donation');
    }
}
